<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");
$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if($arParams["EVENT_NAME"] == '')
    $arParams["EVENT_NAME"] = "FEEDBACK_FORM";
$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
if($arParams["EMAIL_TO"] == '')
    $arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '')
    $arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");

if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] <> '' && (!isset($_POST["PARAMS_HASH"]) || $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
    $arResult["ERROR_MESSAGE"] = array();
    if(check_bitrix_sessid())
    {
        if(empty($arParams["REQUIRED_FIELDS"]) || !in_array("NONE", $arParams["REQUIRED_FIELDS"]))
        {

            if((empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])) && mb_strlen($_POST["MESSAGE"]) <= 5)
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_MESSAGE");
///////////////////////////////////////////////////////////////////////////
            if((empty($arParams["REQUIRED_FIELDS"]) || in_array("NOVAYA_ZAYAVKA", $arParams["REQUIRED_FIELDS"])) && empty($_POST["user_NOVAYA_ZAYAVKA"]) <= 3)
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_NOVAYA_ZAYAVKA");

            if((empty($arParams["REQUIRED_FIELDS"]) || in_array("KATEGORIYA", $arParams["REQUIRED_FIELDS"])) && mb_strlen($_POST["user_KATEGORIYA"]) <= 1)
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_KATEGORIYA");

            if((empty($arParams["REQUIRED_FIELDS"]) || in_array("VID_ZAYAVKI", $arParams["REQUIRED_FIELDS"])) && mb_strlen($_POST["user_VID_ZAYAVKI"]) <= 1)
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_VID_ZAYAVKI");

            if((empty($arParams["REQUIRED_FIELDS"]) || in_array("SKLAD_POSTAVKI", $arParams["REQUIRED_FIELDS"])) && mb_strlen($_POST["user_SKLAD_POSTAVKI"]) <= 1)
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_SKLAD_POSTAVKI");

            if((empty($arParams["REQUIRED_FIELDS"]) || in_array("SOSTAV_ZAYAVKI", $arParams["REQUIRED_FIELDS"])) && mb_strlen($_POST["user_SOSTAV_ZAYAVKI"]) <= 1)
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_SOSTAV_ZAYAVKI");
        
///////////////////////////////////////////////////////////////////////////        
        }
        if(mb_strlen($_POST["user_email"]) > 1 && !check_email($_POST["user_email"]))
            $arResult["ERROR_MESSAGE"][] = GetMessage("MF_EMAIL_NOT_VALID");
        if($arParams["USE_CAPTCHA"] == "Y")
        {
            $captcha_code = $_POST["captcha_sid"];
            $captcha_word = $_POST["captcha_word"];
            $cpt = new CCaptcha();
            $captchaPass = COption::GetOptionString("main", "captcha_password", "");
            if ($captcha_word <> '' && $captcha_code <> '')
            {
                if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass))
                    $arResult["ERROR_MESSAGE"][] = GetMessage("MF_CAPTCHA_WRONG");
            }
            else
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_CAPTHCA_EMPTY");

        }
        if(empty($arResult["ERROR_MESSAGE"]))
        {
            $arFields = Array(
              
///////////////////////////////////////////////////////////////////////////
                "EMAIL_TO" => $arParams["EMAIL_TO"],
                "NOVAYA_ZAYAVKA" => $_POST["user_NOVAYA_ZAYAVKA"],
                "KATEGORIYA" => $_POST["user_KATEGORIYA"],
                "VID_ZAYAVKI" => $_POST["user_VID_ZAYAVKI"],
                "SKLAD_POSTAVKI" => $_POST["user_SKLAD_POSTAVKI"],
                "SOSTAV_ZAYAVKI" => $_POST["user_SOSTAV_ZAYAVKI"],

///////////////////////////////////////////////////////////////////////////
                
                "TEXT" => $_POST["MESSAGE"],
            );
            if(!empty($arParams["EVENT_MESSAGE_ID"]))
            {
                foreach($arParams["EVENT_MESSAGE_ID"] as $v)
                    if(intval($v) > 0)
                        CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", intval($v));
            }
            else
                CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
            $_SESSION["MF_NAME"] = htmlspecialcharsbx($_POST["user_name"]);
            $_SESSION["MF_EMAIL"] = htmlspecialcharsbx($_POST["user_email"]);
            $event = new \Bitrix\Main\Event('main', 'onFeedbackFormSubmit', $arFields);
            $event->send();
            LocalRedirect($APPLICATION->GetCurPageParam("success=".$arResult["PARAMS_HASH"], Array("success")));
        }

        $arResult["MESSAGE"] = htmlspecialcharsbx($_POST["MESSAGE"]);

///////////////////////////////////////////////////////////////////////////
        $arResult["AUTHOR_NOVAYA_ZAYAVKA"] = htmlspecialcharsbx($_POST["user_NOVAYA_ZAYAVKA"]);
        $arResult["AUTHOR_KATEGORIYA"] = htmlspecialcharsbx($_POST["user_KATEGORIYA"]);
        $arResult["AUTHOR_VID_ZAYAVKI"] = htmlspecialcharsbx($_POST["user_VID_ZAYAVKI"]);
        $arResult["AUTHOR_SKLAD_POSTAVKI"] = htmlspecialcharsbx($_POST["user_SKLAD_POSTAVKI"]);
        $arResult["AUTHOR_SOSTAV_ZAYAVKI"] = htmlspecialcharsbx($_POST["user_SOSTAV_ZAYAVKI"]);

///////////////////////////////////////////////////////////////////////////
    }
    else  
        $arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
}
elseif($_REQUEST["success"] == $arResult["PARAMS_HASH"])
{
    $arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
}

if(empty($arResult["ERROR_MESSAGE"]))
{
    if($USER->IsAuthorized())
    {
        $arResult["AUTHOR_NAME"] = $USER->GetFormattedName(false);
        $arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($USER->GetEmail());
    }
    else
    {
        if($_SESSION["MF_NAME"] <> '')
            $arResult["AUTHOR_NAME"] = htmlspecialcharsbx($_SESSION["MF_NAME"]);
        if($_SESSION["MF_EMAIL"] <> '')
            $arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($_SESSION["MF_EMAIL"]);
    }
}

if($arParams["USE_CAPTCHA"] == "Y")
    $arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());

$this->IncludeComponentTemplate();



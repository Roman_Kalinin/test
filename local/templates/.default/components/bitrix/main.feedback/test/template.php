<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="mfeedback">
  <?php if (!empty($arResult["ERROR_MESSAGE"])) {
    foreach ($arResult["ERROR_MESSAGE"] as $v)
      ShowError($v);
  }
  if ($arResult["OK_MESSAGE"] <> '') { ?>
    <div class="mf-ok-text"><?= $arResult["OK_MESSAGE"] ?></div>
  <?php } ?>

  <form action="<?= POST_FORM_ACTION_URI ?>" method="POST">
    <?= bitrix_sessid_post() ?>
    <!---- ---->
    <form method="post" class="form-horizontal">
      <div class="form-group">
        <h4><?= GetMessage("MFT_NOVAYA_ZAYAVKA") ?><?php if (empty($arParams["REQUIRED_FIELDS"]) || in_array("NOVAYA_ZAYAVKA", $arParams["REQUIRED_FIELDS"])) : ?></h4><span class="mf-req"></span><?php endif ?><br>
        <label>Заголовок заявки</label><br>
        <input type="text" name="user_NOVAYA_ZAYAVKA" class="form-control" required>
      </div><br>
      <!---- ---->
      <div class="mf-name">
        <div class="mf-text">
          <h4><?= GetMessage("MFT_KATEGORIYA") ?><?php if (empty($arParams["REQUIRED_FIELDS"]) || in_array("KATEGORIYA", $arParams["REQUIRED_FIELDS"])) : ?></h4><span class="mf-req"></span><?php endif ?>
        </div><br>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="user_KATEGORIYA" value="<?= $arResult["AUTHOR_KATEGORIYA"] ?>" required>
          <label class="form-check-label">Масла, автохимия, фильтры, автоаксессуары, обогреватели, запчасти, сопутствующие товары</label>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="user_KATEGORIYA" value="<?= $arResult["AUTHOR_KATEGORIYA"] ?>">
            <label class="form-check-label">Шины, диски</label>
          </div>
        </div><br>
        <!---- ---->
        <div class="mf-name">
          <div class="mf-text">
            <h4><?= GetMessage("MFT_VID_ZAYAVKI") ?><?php if (empty($arParams["REQUIRED_FIELDS"]) || in_array("VID_ZAYAVKI", $arParams["REQUIRED_FIELDS"])) : ?></h4><span class="mf-req"></span><?php endif ?>
          </div><br>

          <div class="form-check">
            <input class="form-check-input" type="radio" name="user_VID_ZAYAVKI" value="<?= $arResult["AUTHOR_VID_ZAYAVKI"] ?>" required>
            <label class="form-check-label">Запрос цены и сроков поставки</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="user_VID_ZAYAVKI" value="<?= $arResult["AUTHOR_VID_ZAYAVKI"] ?>">
            <label class="form-check-label">Пополнение складов</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="user_VID_ZAYAVKI" value="<?= $arResult["AUTHOR_VID_ZAYAVKI"] ?>">
            <label class="form-check-label">Спецзаказ</label>
          </div>
        </div><br>
        <!---- ---->
        <div class="form-group">
          <h4><?= GetMessage("MFT_SKLAD_POSTAVKI") ?><?php if (empty($arParams["REQUIRED_FIELDS"]) || in_array("SKLAD_POSTAVKI", $arParams["REQUIRED_FIELDS"])) : ?></h4><span class="mf-req"></span><?php endif ?><br><br>
          <select name="user_SKLAD_POSTAVKI" class="form-control" value="<?= $arResult["AUTHOR_VID_ZAYAVKI"] ?>" required>
            <option value="<?= $arResult["AUTHOR_SKLAD_POSTAVKI"] ?>">(выберите склад поставки)</option>
            <option value="<?= $arResult["AUTHOR_SKLAD_POSTAVKI"] ?>">Склад 1</option>
            <option value="<?= $arResult["AUTHOR_SKLAD_POSTAVKI"] ?>">Склад 2</option>
          </select>
        </div><br>
        <!---- ---->
        <div class="container">
          <form action="" method="post">
            <h4><?= GetMessage("MFT_SOSTAV_ZAYAVKI") ?><?php if (empty($arParams["REQUIRED_FIELDS"]) || in_array("SOSTAV_ZAYAVKI", $arParams["REQUIRED_FIELDS"])) : ?></h4><span class="mf-req"></span><?php endif ?><br><br>
            <div class="order-inputs">
              <div class="content">
                <div class="sel">
                  <label for="user_SOSTAV_ZAYAVKI"><font color="red">Бренд</font></label><br>
                  <select name="user_BREND" class="sel1">
                    <option value="<?= $arResult["AUTHOR_SOSTAV_ZAYAVKI"] ?>">Выберите бренд</option>
                    <option value="<?= $arResult["AUTHOR_SOSTAV_ZAYAVKI"] ?>">Бренд 1</option>
                    <option value="<?= $arResult["AUTHOR_SOSTAV_ZAYAVKI"] ?>">Бренд 2</option>
                  </select>
                </div>
                <div class="input-box">
                  <label for="user_SOSTAV_ZAYAVKI" ><font color="red">Наименование</font></label>
                  <input type="text" name="user_SOSTAV_ZAYAVKI" class="form-control" required>
                </div>
                <div class="input-box">
                  <label for="user_SOSTAV_ZAYAVKI"><font color="red">Количество</font></label>
                  <input type="text" name="user_SOSTAV_ZAYAVKI" class="form-control" required>
                </div>
                <div class="input-box">
                  <label for="user_SOSTAV_ZAYAVKI"><font color="red">Фасовка</font></label>
                  <input type="text" name="user_SOSTAV_ZAYAVKI" class="form-control" required>
                </div>
                <div class="input-box">
                  <label for="user_SOSTAV_ZAYAVKI"><font color="red">Клиент</font></label>
                  <div style="display: flex; align-items: center;">
                    <input type="text" name="user_SOSTAV_ZAYAVKI" class="form-control" required>
                    <button type="button" class="btn btn-default add-order-input" style="width:25px; height:25px;  color: white; margin-right: 10px; margin-left: 10px;">+</button>
                    <button type="button" class="btn btn-default delete-order-input" style="width:25px; height:25px;  color: white;">-</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <br>
        <script>
          document.querySelector('.add-order-input').addEventListener('click', function (e) {
            e.preventDefault();
            var orderInputs = document.querySelector('.order-inputs');
            var clone = orderInputs.cloneNode(true);
            document.querySelector('.order-inputs').parentNode.insertBefore(clone, orderInputs.nextSibling);
          });

          document.querySelector('.delete-order-input').addEventListener('click', function () {
            var orderInputs = document.querySelectorAll('.order-inputs');
            if (orderInputs.length > 1) {
              orderInputs[orderInputs.length - 1].remove();
            }
          });
        </script><br>
        <form method="post" class="form-horizontal" enctype="multipart/form-data">
          <div class="form-group">
            <input type="file" name="file" class="form-control-file">
          </div>
        </form><br>
        <!---- ---->
        <div class="mf-message">
          <div class="mf-text">
            <?= GetMessage("MFT_MESSAGE") ?><?php if (empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])) : ?><span class="mf-req"></span><?php endif ?>
          </div>
          <textarea name="MESSAGE" rows="7" cols="60"><?= $arResult["MESSAGE"] ?>...</textarea>
        </div><br>


        <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
        <input type="submit" name="submit" value="<?= GetMessage("MFT_SUBMIT") ?>">
      </form>
    </div>
